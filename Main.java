import java.util.Scanner;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args){
        LihatMenuAwal();
    }
    public static String Input(String input){
        System.out.print(input + ": ");
        String data = scanner.nextLine();
        return data;
    }

    // Menu Awal
    public static void LihatMenuAwal(){
        System.out.println("--------------------------------------");
        System.out.println("Kalkulator Penghitung Luas Dan Volume");
        System.out.println("--------------------------------------");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volume");
        System.out.println("0. Tutup Aplikasi");
        String pilihan = Input("Tentukan pilihanmu");
        if (pilihan.equals("1")){
            LihatMenuLuas();
        }
        else if (pilihan.equals("2")){
            LihatMenuVolume();
        }
        else if (pilihan.equals("0")){
            System.exit(0);
        }
        else {
            System.out.println("Mohon Masukkan pilihan yang benar");
            LihatMenuAwal();
        }
    }

    // Menu Luas
    public static void LihatMenuLuas(){
        System.out.println("------------------------------------");
        System.out.println("Pilih Bidang yang akan dihitung");
        System.out.println("------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("0. Kembali ke menu sebelumnya");
        String pilihan = Input("Ayok dipilih");
        if (pilihan.equals("1")){
            LihatMenuPersegi();
        }
        else if (pilihan.equals("2")){
            LihatMenuLingkaran();
        }
        else if (pilihan.equals("3")){
            LihatMenuSegitiga();
        }
        else if (pilihan.equals("4")){
            LihatMenuPP();
        }
        else if (pilihan.equals("0")){
            LihatMenuAwal();
        }
        else {
            System.out.println("Masukkan Pilihan yang benar");
            LihatMenuLuas();
        }
    }

    // Menu Volume
    public static void LihatMenuVolume(){
        System.out.println("------------------------------------");
        System.out.println("Pilih Volume yang akan dHitung");
        System.out.println("------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke menu sebelumnya");
        String pilihan = Input("Ayang mau yang mana");
        if (pilihan.equals("1")){
            LihatMenuKubus();
        }
        else if (pilihan.equals("2")){
            LihatMenuBalok();
        }
        else if (pilihan.equals("3")){
            LihatMenuTabung();
        }
        else if (pilihan.equals("0")){
            LihatMenuAwal();
        }
        else {
            System.out.println("Masukkan Pilihan yang benar");
            LihatMenuVolume();
        }

    }

    // Menu Persegi
    public static void LihatMenuPersegi(){
        System.out.println("---------------------------------");
        System.out.println("Anda Memilih Persegi");
        System.out.println("---------------------------------");
        System.out.print("Masukkan Sisi : ");
        int sisi = scanner.nextInt();
        int luas = sisi * sisi;
        System.out.println("Luas Persegi Adalah :" + luas);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        LihatMenuAwal();
    }

    // Menu Lingkaran
    public static void LihatMenuLingkaran(){
        double luas, phi = 3.14;
        int r;
        System.out.println("---------------------------------");
        System.out.println("Anda Memilih Lingkaran");
        System.out.println("---------------------------------");
        System.out.println("Masukan Jari-Jari : ");
        r = scanner.nextInt();
        luas = phi*r*r;
        System.out.println("Luas Lingkaran Adalah :" + luas);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        LihatMenuAwal();
    }
    // Menu Segitiga
    public static void LihatMenuSegitiga(){
        int alas, tinggi;
        double luas;
        System.out.println("---------------------------------");
        System.out.println("Anda Memilih Segitiga");
        System.out.println("---------------------------------");
        System.out.print("Masukkan Alas : ");
        alas = scanner.nextInt();
        System.out.print("Masukkan Tinggi : ");
        tinggi = scanner.nextInt();
        luas = (alas * tinggi) * 0.5;
        System.out.println("Luas Segitiga Adalah :" + luas);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        LihatMenuAwal();
    }

    // Menu Persegi Panjang
    public static void LihatMenuPP(){
        System.out.println("---------------------------------");
        System.out.println("Anda Memilih Persegi Panjang");
        System.out.println("---------------------------------");
        System.out.print("Masukkan Panjang : ");
        int panjang = scanner.nextInt();
        System.out.print("Masukkan Lebar : ");
        int lebar = scanner.nextInt();
        int luas = panjang * lebar;
        System.out.println("Luas Persegi Panjang Adalah :" + luas);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        LihatMenuAwal();
    }

    // Kubus
    public static void LihatMenuKubus(){
        System.out.println("---------------------------------");
        System.out.println("Anda Memilih Kubus");
        System.out.println("---------------------------------");
        System.out.print("Masukkan Sisi : ");
        int sisi = scanner.nextInt();
        int luas = sisi * sisi * sisi;
        System.out.println("Volume Kubus Adalah :" + luas);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        LihatMenuAwal();
    }

    // Balok
    public static void LihatMenuBalok(){
        System.out.println("---------------------------------");
        System.out.println("Anda Memilih Balok");
        System.out.println("---------------------------------");
        System.out.print("Masukkan Panjang : ");
        int panjang = scanner.nextInt();
        System.out.print("Masukkan Lebar : ");
        int lebar = scanner.nextInt();
        System.out.print("Masukkan Tinggi : ");
        int tinggi = scanner.nextInt();
        int luas = panjang * lebar * tinggi;
        System.out.println("Volume Balok Adalah :" + luas);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        LihatMenuAwal();
    }

    // Tabung
    public static void LihatMenuTabung(){
        double luas, phi = 3.14;
        int r;
        System.out.println("---------------------------------");
        System.out.println("Anda Memilih Tabung");
        System.out.println("---------------------------------");
        System.out.println("Masukan Jari-Jari : ");
        r = scanner.nextInt();
        System.out.print("Masukkan Tinggi : ");
        int tinggi = scanner.nextInt();
        luas = phi*tinggi*r*r;
        System.out.println("Volume Tabung Adalah :" + luas);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        LihatMenuAwal();
    }
}
